import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ButtonsFenster extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ButtonsFenster frame = new ButtonsFenster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ButtonsFenster() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				for(int i = 0; i < 150; i++){
					Random rand = new Random();
					int r = rand.nextInt(256);
					int g = rand.nextInt(256);
					int b = rand.nextInt(256);
					Color farbe = new Color(r,g,b);
					ButtonsFester frame = new ButtonsFester(farbe);
					frame.setVisible(true);
					}
			}
		});
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setAlwaysOnTop(true);
		
		JLabel lblFrage= new JLabel("Wollen Sie ein paar Buttons?"); 
		lblFrage.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblFrage.setBounds(78, 39, 288, 49);
		contentPane.add(lblFrage);
		
		JButton btnJa = new JButton("Ja");
		btnJa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0; i < 50; i++){
				Random rand = new Random();
				int r = rand.nextInt(256);
				int g = rand.nextInt(256);
				int b = rand.nextInt(256);
				Color farbe = new Color(r,g,b);
				ButtonsFester frame = new ButtonsFester(farbe);
				frame.setVisible(true);
				}
			}
		});
		btnJa.setBounds(26, 139, 97, 25);
		contentPane.add(btnJa);
		
		JButton btnNen = new JButton("Nein");
		btnNen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0; i < 100; i++){
					Random rand = new Random();
					int r = rand.nextInt(256);
					int g = rand.nextInt(256);
					int b = rand.nextInt(256);
					Color farbe = new Color(r,g,b);
					ButtonsFester frame = new ButtonsFester(farbe);
					frame.setVisible(true);
					}
			}
		});
		btnNen.setBounds(156, 139, 97, 25);
		contentPane.add(btnNen);
		
		JButton btnVielleicht = new JButton("Vielleicht");
		btnVielleicht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0; i < 75; i++){
					Random rand = new Random();
					int r = rand.nextInt(256);
					int g = rand.nextInt(256);
					int b = rand.nextInt(256);
					Color farbe = new Color(r,g,b);
					ButtonsFester frame = new ButtonsFester(farbe);
					frame.setVisible(true);
					}
			}
		});
		btnVielleicht.setBounds(293, 139, 97, 25);
		contentPane.add(btnVielleicht);
	}
}
