import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class Caesar extends JFrame{
	private String Ergebnis = "";
	public Caesar() {
		getContentPane().setLayout(null);
		setBounds(100, 100, 931, 300);
		JLabel lblNewLabel = new JLabel("Geben Sie hier das zu verschl\u00FCsselnde Wort ein");
		lblNewLabel.setBounds(29, 23, 327, 14);
		getContentPane().add(lblNewLabel);
		
		JTextPane txtAusgabe2 = new JTextPane();
		txtAusgabe2.setBounds(85, 214, 820, 20);
		getContentPane().add(txtAusgabe2);
		
		JTextPane txtAusgabe1 = new JTextPane();
		txtAusgabe1.setBounds(85, 105, 820, 20);
		getContentPane().add(txtAusgabe1);
		
		textField = new JTextField();
		textField.setBounds(29, 40, 876, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Ergebnis:");
		lblNewLabel_1.setBounds(29, 105, 82, 14);
		getContentPane().add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Umwandeln");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Wort = textField.getText();
				Wort = Wort.toLowerCase();
				Ergebnis = "";
				for (char c: Wort.toCharArray()){
					
					int schluessel = 12;
					if (c == ' ') {
						Ergebnis = Ergebnis + " ";
					}else {
						int wert = ((c + schluessel - 97) %26) + 97;
						//int wert = ((c - schluessel - 71) %26) + 97;
						
						char neuesZeichen = (char) (wert);
						Ergebnis = Ergebnis + neuesZeichen;
					}	
					
				}
				txtAusgabe1.setText(Ergebnis);
			}
		});
		btnNewButton.setBounds(29, 71, 120, 23);
		getContentPane().add(btnNewButton);
		
		
		///---------------------------------------------------------
		
		JLabel lblGebenSieHier = new JLabel("Geben Sie hier das verschl\u00FCsselte Wort ein");
		lblGebenSieHier.setBounds(29, 130, 327, 14);
		getContentPane().add(lblGebenSieHier);
		
		JLabel lblNewLabel_1_1 = new JLabel("Ergebnis:");
		lblNewLabel_1_1.setBounds(29, 214, 82, 14);
		getContentPane().add(lblNewLabel_1_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(29, 155, 876, 20);
		getContentPane().add(textField_1);
		
		JButton btnNewButton_1 = new JButton("Umwandeln");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Wort = textField_1.getText();
				Wort = Wort.toLowerCase();
				Ergebnis = "";
				for (char c: Wort.toCharArray()){
					
					int schluessel = 12;
					if (c == ' ') {
						Ergebnis = Ergebnis + " ";
					}else {
						int wert = ((c - schluessel - 71) %26) + 97;
						
						char neuesZeichen = (char) (wert);
						Ergebnis = Ergebnis + neuesZeichen;
					}	
					
				}
				txtAusgabe2.setText(Ergebnis);
				
			}
		});
		btnNewButton_1.setBounds(29, 180, 120, 23);
		getContentPane().add(btnNewButton_1);
		
	}
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Caesar frame = new Caesar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
