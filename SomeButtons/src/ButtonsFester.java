import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ButtonsFester extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Random rand = new Random();
		int r = rand.nextInt(256);
		int g = rand.nextInt(256);
		int b = rand.nextInt(256);
		Color farbe = new Color(r,g,b);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ButtonsFester frame = new ButtonsFester(farbe);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ButtonsFester(Color neueFarbe) {
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Random rand = new Random();
		int x = rand.nextInt(1400);
		int y = rand.nextInt(800);
		setBounds(x, y, 281, 193);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setAlwaysOnTop(true);
		setContentPane(contentPane);
		
		JButton btnKlickMich = new JButton("Klick mich!");
		btnKlickMich.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				//Random rand = new Random();
				//int x = rand.nextInt(1400);
				//int y = rand.nextInt(800);
				//setBounds(x, y, 281, 193);
			}
			@Override
			public void mouseClicked(MouseEvent e) { 
				for(int i = 0; i < 10; i++) {
					Random rand = new Random();
					int r = rand.nextInt(256);
					int g = rand.nextInt(256);
					int b = rand.nextInt(256);
					Color farbe = new Color(r,g,b);
					ButtonsFester frame = new ButtonsFester(farbe);
					frame.setVisible(true);
				}
			}
		});
		btnKlickMich.setBackground(neueFarbe);
		btnKlickMich.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		contentPane.add(btnKlickMich, BorderLayout.CENTER);
	}

}
